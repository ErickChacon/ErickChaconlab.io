---
title: Simple Integration Between Markdown and Latex
shortitle: SimpleDown
date: '2015-07-23'
categories:
  - Software
tags:
  - Markdown
  - Latex
image: software/simple-notes.png
weight: 10
link: https://github.com/ErickChacon/research-notes
---

SimpleDown is pandoc template with less dependancies as possible for compatibility
with LaTex clases.
