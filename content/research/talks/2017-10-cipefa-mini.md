---
title: Spatio-Temporal Models in Epidemiology
date: '2017-10-07'
categories:
  - modelling
tags:
  - extreme events
image: spp.png
event: I International Conference of Stochastic Processes, Random Phenomena and their Applications
location: Universidad Nacional de Ingenierı́a
---

<!-- Effects of Extreme Hydro-Climatic Events on Birth Weight -->
