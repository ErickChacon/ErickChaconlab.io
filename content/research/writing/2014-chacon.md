---
categories:
  - Example
tags:
  - Markdown
image: chicas.png
weight: 10
references:
- id: ChaconMontalvan2014
  type: thesis
  author:
  - family: Chacon Montalvan
    given: Erick A.
  issued:
  - year: '2014'
  title: <span class="nocase">Prediction of Sedimentary Deposition Environments Based
    on Bathymetrical, Morphological and Sedimentary Characteristics in the Peruvian
    Continental Edge Using Techniques in Spatial Statistics</span>
  publisher: Universidad Nacional de Ingenierı́a
  page: '124'
  genre: PhD thesis
  abstract: Biotic and abiotic organisms in the marine habitat erode particle fragments
    that are accu- mulated in strategic locations, called environments of deposition,
    characterized by the structure of the topography and the ocean dynamics. Thin
    sediments, useful to obtain high-quality sedimenta- ry information, are generally
    accumulated in areas favoring the continuous deposition of sediments. These environments
    are characterized by the morphology of the marine topography and the behavior
    of certain external agents such as hydrodynamic energy, winds, marine currents,
    and so on. Knowl- edge of these areas offers significant contributions for the
    development of in situ geological work and it is the basis for research studies
    in the northern Humboldt Current System (HCS) in Peru, which is the marine ecosystem
    with the most extreme conditions and unknowns. Therefore, in order to predict
    the location of continuous deposition environments in Peruvian Continental Margin,
    the presence of continuous deposition environments are modeled spatially based
    on the morphology and bathymetry characteristics. In the study, high-resolution
    grids of bathymetry and likely continuous deposition environments were obtained.
    The logistic kriging model confirms the association between continuous deposition
    environments and morphological characteristics of the seabed such as depth, slope,
    distance to the coast, among others.
  keyword: Bathymetry,Continuous sedimentation environments,Geostatistics,Kriging,Morphology,Spatial
    Correlation.
---
