---
title: Thesis Template in Latex (UNI)
date: 2014-09-18 01:31:17.000000000 +01:00
categories:
- Ciencia
- Education
- Estadística
- LaTeX
- Scientific Writing
- Thesis
- Universidad Nacional de Ingeniería
tags:
- Ciencia
- Education
- LaTeX
- Scientific Writing
- Thesis
- Universidad Nacional de Ingeniería
---
<p style="text-align:justify;">Some months ago, I finished my undergraduate thesis and I modified the ClemsonThesis project made by Andrew R. Dalton in order to customize and create the <strong>UniThesis.cls</strong> class in <em>LaTeX</em> as a template for undergraduate tesis at <strong><a title="Universidad Nacional de Ingeniería" href="http://www.uni.edu.pe/" target="_blank">Universidad Nacional de Ingeniería</a> (UNI)</strong>. The template has the features required by UNI, but can be used by other universities students modifying their personal information. Furthermore, this project is updated in <a title="github" href="https://github.com/" target="_blank">github</a> and you can download and use it  through the following link:</p>
<blockquote style="border:2px solid #666;padding:8px;background-color:#ccc;">
<p style="text-align:center;">"<a title="UniThesis Template Project" href="https://github.com/ErickChacon/UniThesis" target="_blank">UniThesis Template Project</a>"</p>
</blockquote>
<p style="text-align:justify;">I really suggest you to use this template if you are a UNI student and have curiosity to learn <a title="LaTeX" href="http://www.latex-project.org/" target="_blank">LaTeX,</a> please do not hesitate to make me any question.</p>

<img src="/images/frontmatter.jpg" alt="frontmatter">
