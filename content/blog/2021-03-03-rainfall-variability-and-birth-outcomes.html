---
title: |
    Methodological notes about the study:
    'Rainfall variability and adverse birth outcomes in Amazonia'
author: "Erick A. Chacón-Montalván"
date: 2021-03-03
categories:
    - Amazonia
    - Public health
tags:
    - Newborn health
    - Bayesian statistics
    - Climate change
bibliography: 2021-03-03-rainfall-variability.bib
---



<p><img src="/blog/2021-03-03-rainfall-variability-and-birth-outcomes_myfiles/fig1-weekly-mbsi-per-muni.jpg" /></p>
<p>Our recent study, published at <a href="https://doi.org/10.1038/s41893-021-00684-9">Nature
Sustainability</a>, shows the adverse impacts of
extreme (and not so extreme) rainfall events on newborns’ health. The results were
obtained after analysing almost 300,000 live births during 2006-2017 in river-dependent
Municipalities of the Brazilian Amazonia <span class="citation">(Chacón-Montalván et al. 2021)</span>. We found impacts
on preterm birth (PTB), low birth weight (LBW), and birth weight (BW); indicating that the
effects of extreme rainfall events can be due to preterm birth or restricted foetal
growth. More details about these results can be found at <a href="https://www.lancaster.ac.uk/lec/about-us/news/new-research-highlights-health-risks-to-babies-on-the-front-line-of-climate-change">Lancaster University
news</a>.
In this post, I provide some details about our statistical modelling approach (Fig. 1) to
give insights into why we were able to identify these effects.</p>
<div class="figure">
<img src="/blog/2021-03-03-rainfall-variability-and-birth-outcomes_myfiles/edfig02-modeling-diagram.jpg" alt="" />
<p class="caption"><strong>Figure 1</strong>: Methodological diagram of the study ‘Rainfall variability and adverse birth outcomes in Amazonia’</p>
</div>
<div id="exposure-measurement" class="section level2">
<h2>Exposure measurement</h2>
<p>To evaluate the impacts of climatic variability on human health, it is relevant to
properly define and measure <em>exposure</em>. In our case, rainfall variability exposure during
pregnancy should consider location, period, metric, and summarised metrics.</p>
<ul>
<li><em>Location</em>: Climatic conditions are spatially-varying, for this reason, it is fundamental
to consider the area of exposure for the mother. We defined this as the municipality of
residence because an extreme event does not necessarily have direct impacts on her
health, but could affect the economic and health system of the mother’s Municipality,
and consequently mother’s nutrition.</li>
<li><em>Period</em>: We did not consider only the pregnancy period, but also the pre-pregnancy
period because of the importance of the mother’s health status at conception date.</li>
<li><em>Metric</em>: For a specific time and location, a classical approach would consider using
the absolute rainfall level. However, we believe that this is not an adequate metric to
evaluate the effects of rainfall because people used to high rainfall levels are likely
to learn to cope with it. For this reason, we use a model-based standardised index
(MBSI), applied to rainfall, that captures extremeness with respect to the long-term
seasonal pattern. A 0-value would mean that the rainfall level is the same as the
seasonal pattern, while a positive (negative) value would indicate intense (deficient)
rainfall concerning the seasonal pattern. See <span class="citation">Chacón-Montalván et al. (2019)</span> for
more details about the MBSI.</li>
<li><em>Summarised metric</em>: For a specific mother, we obtain a time series of the exposure
<em>metric</em>. We need to apply a <em>function</em> to this MBSI time-series to obtain a summarised
metric because we only have outcomes (e.g. birth weight) at the end of the pregnancy.
This function should help us to identify different types of exposure. Using the mean
function would be useless because deficit and intense events would be countered. It is
more adequate to use bivariate functions (<span class="math inline">\(h_1\)</span>, <span class="math inline">\(h_2\)</span>) that summarise only intense
(<span class="math inline">\(h_1\)</span>) and only deficient (<span class="math inline">\(h_2\)</span>) events. We used this approach to define three types
of exposure capturing (i) deviation from seasonality, (ii) non-extreme events (&gt; 1 s.d.,
but &lt; 1.96 s.d.), and (iii) extreme events (<span class="math inline">\(\geq\)</span> 1.96 s.d.).</li>
</ul>
</div>
<div id="statistical-modelling" class="section level2">
<h2>Statistical modelling</h2>
<p>Specific details of our models can be found in the methods section of our paper. Here, I
will answer four questions that were relevant to decide the type and structure of these
models.</p>
<ul>
<li><em>Should we model only the mean parameter?</em>: This question is only related to our BW
models given that the assumed distributions have mean and scale parameters. We have seen
heteroscedasticity of birth-weight with respect to Municipality, age, and other
covariates, highlighting the importance of modelling the scale parameter. In general, we
observed better fitting when modelling both parameters (mean and scale). It was
straightforward to do using <em>Bayesian additive models for location, scale, and shape</em>
implemented by <span class="citation">Umlauf, Klein, and Zeileis (2018)</span> in the <a href="http://bamlss.org/">bamlss</a> package.</li>
<li><em>What variables should we control for?</em>: It depends on the effects that are desired to
disentangle. For example, if you control for gestational age (GA) in a BW model, you
obtain estimates of the effects of rainfall events on BW due to restricted foetus
growth; while if you do not control for GA, you obtain estimates of the full effects due
to either restricted foetus growth or gestational age. Similarly, we should not control
for mosquito-borne diseases in case we want to obtain estimates of the full effects.
Although we did not use causal models, we used directed acyclic graphs (DAG) to
understand when we should control for certain covariates or not. More details about DAGs
in causal inference can be found at <span class="citation">Pearl, Glymour, and Jewell (2016)</span>. <a href="http://dagitty.net/">DAGitty</a>
is a great tool to explore the adjustments required.</li>
<li><em>What type of relationships to use?</em>: In real-world applications, linear relationships
are not common, and birth weight analysis is not an exception. We observed that age, for
instance, has a non-linear relationship with a peak around 35 years. Other complex
relationships have been observed with respect to other covariates. For this reason, we
preferred to use additive non-linear effects which are implemented in packages like
<a href="https://cran.r-project.org/web/packages/mgcv/index.html">mgcv</a> and
<a href="http://bamlss.org/">bamlss</a>.</li>
<li><em>How to include the summarised metrics of exposure?</em>: As mentioned in the previous
section we used three types of bivariate metrics of exposure where the first and second
elements are related to deficient and intense rainfall events respectively. As expected,
both elements are correlated and precaution should be taken when including them in the
models. In case, we include the two elements independently, the associated estimated
effects would not be interpretable due to their correlation. It is more appropriate to
include the effects jointly to be able to estimate the effects according to different
combinations for indices. We achieved this by using bivariate non-linear effects.</li>
</ul>
</div>
<div id="future-research" class="section level2">
<h2>Future research</h2>
<ul>
<li><em>Causal mechanisms</em>: While it is clear that rainfall variability can have adverse
impacts on newborns’ health; we still need to learn about the reason and causal
mechanisms that are supported with empirical data. This will require the use of causal
models for observational data to be able to disentangle the effects through different
pathways (e.g. crop yields and quality; vector-borne diseases; and shock-related stress,
anxiety, or mental health). We encourage researchers to focus on this approach to get
better insights into the mechanisms of how rainfall variability impacts newborns’
health.</li>
<li><em>Varying exposure susceptibility during pregnancy</em>: An implicit assumption of our
approach is that exposure to climatic variability has the same importance at any time of
pregnancy; however, there are likely periods (e.g. first trimester) in which mothers are
more susceptible. We can use a <em>summarised metric</em> that can capture this pattern by
using smooth weighting functions. Parameters of these functions can be estimated from
the data, but it can be possible to compare sensible hypothetical curves.</li>
<li><em>Evaluate the exposure of other environmental and climatic conditions</em>: Our approach can
be applied to similar problems in which the goal is to assess the impacts of climatic or
environmental events on human health (e.g. birth-weight, mortality, respiratory
diseases). We can for example evaluate the impacts of exposure to extreme temperature,
poor air quality, and so on.</li>
</ul>
</div>
<div id="references" class="section level2 unnumbered">
<h2>References</h2>
<div id="refs" class="references">
<div id="ref-chacon-montalvan2019modelbased">
<p>Chacón-Montalván, Erick A., Luke Parry, Gemma Davies, and Benjamin M. Taylor. 2019. “A Model-Based General Alternative to the Standardised Precipitation Index,” June. <a href="https://arxiv.org/abs/1906.07505v1">https://arxiv.org/abs/1906.07505v1</a>.</p>
</div>
<div id="ref-chacon-montalvan2021rainfall">
<p>Chacón-Montalván, Erick A., Benjamin M. Taylor, Marcelo G. Cunha, Gemma Davies, Jesem D. Y. Orellana, and Luke Parry. 2021. “Rainfall Variability and Adverse Birth Outcomes in Amazonia.” <em>Nature Sustainability</em>, March, 1–12. <a href="https://doi.org/10.1038/s41893-021-00684-9">https://doi.org/10.1038/s41893-021-00684-9</a>.</p>
</div>
<div id="ref-pearl2016causal">
<p>Pearl, Judea, Madelyn Glymour, and Nicholas P Jewell. 2016. <em>Causal Inference in Statistics: A Primer</em>. John Wiley &amp; Sons.</p>
</div>
<div id="ref-umlauf2018bamlss">
<p>Umlauf, Nikolaus, Nadja Klein, and Achim Zeileis. 2018. “BAMLSS: Bayesian Additive Models for Location, Scale, and Shape (and Beyond).” <em>Journal of Computational and Graphical Statistics</em> 27 (3): 612–27. <a href="https://doi.org/10.1080/10618600.2017.1407325">https://doi.org/10.1080/10618600.2017.1407325</a>.</p>
</div>
</div>
</div>
